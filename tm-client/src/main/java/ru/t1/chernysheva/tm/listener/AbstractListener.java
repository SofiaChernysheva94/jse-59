package ru.t1.chernysheva.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.IListener;
import ru.t1.chernysheva.tm.api.model.ICommand;
import ru.t1.chernysheva.tm.api.service.ITokenService;
import ru.t1.chernysheva.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    public abstract void execute(@NotNull final ConsoleEvent consoleEvent);

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
