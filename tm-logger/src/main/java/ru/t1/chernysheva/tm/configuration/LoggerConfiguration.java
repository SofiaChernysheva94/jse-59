package ru.t1.chernysheva.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.chernysheva.tm.service.PropertyLoggerService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.chernysheva.tm")
public class LoggerConfiguration {

    @NotNull PropertyLoggerService propertyLoggerService = new PropertyLoggerService();

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(propertyLoggerService.getJmsBrokerUrl());
        factory.setTrustAllPackages(true);
        return factory;
    }

}
