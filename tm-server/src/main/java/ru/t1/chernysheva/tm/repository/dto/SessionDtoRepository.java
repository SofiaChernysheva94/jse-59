package ru.t1.chernysheva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final SessionDTO model) {
        entityManager.remove(model);
    }

}
