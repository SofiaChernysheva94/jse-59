package ru.t1.chernysheva.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.dto.IDtoRepository;
import ru.t1.chernysheva.tm.dto.model.AbstractModelDTO;
import ru.t1.chernysheva.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    final static String HINT = "org.hibernate.cacheable";

    protected abstract Class<M> getEntityClass();

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void clear() {
        for (final M model : findAll())
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass())
                .setHint(HINT, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Sort sort) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        @NotNull String orderStatement = " ORDER BY m." + (sort != null ? sort.getSortField() : Sort.BY_CREATED.getSortField());
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass()).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass())
                .setMaxResults(index)
                .getResultStream()
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        @NotNull String query = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull String query = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id";
        entityManager.createQuery(query).setParameter("id", id).executeUpdate();
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
