package ru.t1.chernysheva.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.chernysheva.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.chernysheva.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> implements IUserOwnedDtoRepository<M> {


    @NotNull
    @Getter
    @PersistenceContext
    protected EntityManager entityManager;


    protected abstract Class<M> getEntityClass();

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (!userId.equals(model.getUserId())) model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        for (final M model : findAll(userId))
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        @NotNull String orderStatement = " ORDER BY m." + (sort != null ? sort.getSortField() : Sort.BY_CREATED.getSortField());
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .setMaxResults(index)
                .getResultStream()
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull String query = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(query, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        removeById(userId, model.getId());
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull String query = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id AND m.userId = :userId";
        entityManager.createQuery(query).setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
