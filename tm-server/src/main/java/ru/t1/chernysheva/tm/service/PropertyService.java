package ru.t1.chernysheva.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @Value("#{environment['application.name']}")
    public String applicationName;

    @Value("#{environment['application.log']}")
    public String applicationLog;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['database.url']}")
    private String dBUrl;

    @Value("#{environment['database.password']}")
    private String dBPassword;

    @Value("#{environment['database.username']}")
    private String dBUser;

    @Value("#{environment['database.schema']}")
    private String dBSchema;

    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @Value("#{environment['database.l2Cache']}")
    private String dBL2Cache;

    @Value("#{environment['database.dialect']}")
    private String dBDialect;

    @Value("#{environment['database.showSQL']}")
    private String dBShowSQL;

    @Value("#{environment['database.hbm2ddl']}")
    private String dBHbm2DDL;

    @Value("#{environment['database.cacheRegionClass']}")
    private String dBCacheRegion;

    @Value("#{environment['database.useQueryCache']}")
    private String dBQueryCache;

    @Value("#{environment['database.useMinPuts']}")
    private String dBMinimalPuts;

    @Value("#{environment['database.regionPrefix']}")
    private String dBCacheRegionPrefix;

    @Value("#{environment['database.configFilePath']}")
    private String dBCacheProvider;

    @Value("#{environment['jms.brokerUrl']}")
    private String jMSBrokerURL;

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

}
