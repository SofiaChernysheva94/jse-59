package ru.t1.chernysheva.tm.exception.system;

public class AuthenticationException extends AbstractSystemException {

    public AuthenticationException() {
        super("Authentication failed...");
    }

}
